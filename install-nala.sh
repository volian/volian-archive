#!/bin/bash

set -e

base_url="https://deb.volian.org/volian/pool/main/v/volian-archive/"

version="0.3.1"

archive="volian-archive-nala_${version}_all.deb"
keyring="volian-archive-keyring_${version}_all.deb"

wget "${base_url}${archive}" -P /tmp
wget "${base_url}${keyring}" -P /tmp

echo "sudo is required to install the archives, update apt, and install Nala"

sudo apt-get install /tmp/${archive} /tmp/${keyring} -y
sudo apt-get update
sudo apt-get install nala -y
