# Volian Linux Scar Sources

This repo contains the Volian Linux Scar Sources, Preferences and Keyring.

No matter your use case, you should first install the keyring:

`sudo apt install ./volian-archive-keyring.deb`

If you intend to only install `nala` you should use:

`volian-archive-nala.deb`

This will prevent software from being updated from the Scar repository and overwriting distro packages.

If you would like to take benefit of newer software you can use:

`volian-archive-scar.deb`

This package has no such restrictions.
Although it may work with other releases,
`volian-archive-scar.deb` should only be used on Debian Sid
